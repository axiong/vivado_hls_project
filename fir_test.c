/*******************************************************************************
Vendor: Xilinx
Associated Filename: fir_tes.c
Purpose: Vivado HLS Tutorial Example
Device: All
Revision History: May 30, 2008 - initial release

*******************************************************************************
Copyright 2008 - 2012 Xilinx, Inc. All rights reserved.

This file contains confidential and proprietary information of Xilinx, Inc. and
is protected under U.S. and international copyright and other intellectual
property laws.

DISCLAIMER
This disclaimer is not a license and does not grant any rights to the materials
distributed herewith. Except as otherwise provided in a valid license issued to
you by Xilinx, and to the maximum extent permitted by applicable law:
(1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND WITH ALL FAULTS, AND XILINX
HEREBY DISCLAIMS ALL WARRANTIES AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY,
INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT, OR
FITNESS FOR ANY PARTICULAR PURPOSE; and (2) Xilinx shall not be liable (whether
in contract or tort, including negligence, or under any other theory of
liability) for any loss or damage of any kind or nature related to, arising under
or in connection with these materials, including for any direct, or any indirect,
special, incidental, or consequential loss or damage (including loss of data,
profits, goodwill, or any type of loss or damage suffered as a result of any
action brought by a third party) even if such damage or loss was reasonably
foreseeable or Xilinx had been advised of the possibility of the same.

CRITICAL APPLICATIONS
Xilinx products are not designed or intended to be fail-safe, or for use in any
application requiring fail-safe performance, such as life-support or safety
devices or systems, Class III medical devices, nuclear facilities, applications
related to the deployment of airbags, or any other applications that could lead
to death, personal injury, or severe property or environmental damage
(individually and collectively, "Critical Applications"). Customer assumes the
sole risk and liability of any use of Xilinx products in Critical Applications,
subject only to applicable laws and regulations governing limitations on product
liability.

THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE AT
ALL TIMES.

*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "selector.h"

int main () {

    //cout << "hello" <<endl;
    //vector <int> v;

	  const int NEvent = 20;                          //Total number of events to be processed

	  FILE *Jet_n = fopen("n.dat","rb");              //File with Total number of jets/Event
	  int jet_n[NEvent];                              //The array to hold Total number of jets per Event
	  if (Jet_n==NULL) {fputs ("Jet number file wasn't opened !",stderr);exit(1);}
	  FILE *Jet_pt = fopen("pt.dat","rb");            //In put file with Transverse momentum
	  if (Jet_pt==NULL) {fputs ("Jet pt file wasn't opened !",stderr);exit(1);}

	  int count20;                                    //Number of jets with pt>20GeV

	  //Reading the number of jets for each event
	  fseek(Jet_n, 0, SEEK_SET);
	  for (int k =0; k <NEvent; k++){
		  fscanf(Jet_n,"%i\n", &jet_n[k]);
		  printf("%i\t", jet_n[k]);
	  }
	  fclose(Jet_n);
	  printf("\n");


//Reading full event pt data #################

	  fseek(Jet_pt, 0, SEEK_SET);
	  FILE *f_count_out=fopen("out_count.dat","w");   //output file

	  EVENT_LOOP: for (int i = 0; i < NEvent; i ++){
		  printf("Event %i: ", i);
		  float pt_array [max_jet_n];                 //fixed size array

		  SINGLE_ARRAY_LOOP: for (int a =0; a < jet_n[i]; a++) {
				fscanf(Jet_pt,"%f\n", & pt_array[a]);
				printf("pt: %f\t", pt_array[a]);

		  }
		  sel (&count20,pt_array,jet_n[i]);  //Function to be synthesized that Counts the number of jets

		  fprintf(f_count_out, "%i %i\n", i, count20);
		  printf("\n");
		  printf("The number of jets with Pt larger than 20GeV is %i out of %i jets \n", count20, jet_n[i]);
		  printf("\n");

		  //Clear array elements and the counter
		  for (int j = 0; j < max_jet_n; j ++) { pt_array[j] = 0.0;}
	  }

	  fclose(f_count_out);
	  fclose(Jet_pt);

// End reading full events ########################


// Compare count with the golden output
  printf ("Comparing against output data \n");
  if (system("diff count_gold.dat out_count.dat ")) {

	fprintf(stdout, "*******************************************\n");
	fprintf(stdout, "FAIL: Output DOES NOT match the golden output\n");
	fprintf(stdout, "*******************************************\n");
     return 1;
  } else {
	fprintf(stdout, "*******************************************\n");
	fprintf(stdout, "PASS: The output matches the golden output!\n");
	fprintf(stdout, "*******************************************\n");
     return 0;
  }

}
