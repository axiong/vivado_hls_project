############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
############################################################
open_project selector_prj
set_top sel
add_files selector.c
add_files -tb fir_test.c -cflags "-Wno-unknown-pragmas -Wno-unknown-pragmas"
add_files -tb data/n.dat -cflags "-Wno-unknown-pragmas -Wno-unknown-pragmas"
add_files -tb data/pt.dat -cflags "-Wno-unknown-pragmas -Wno-unknown-pragmas"
add_files -tb data/count_gold.dat -cflags "-Wno-unknown-pragmas -Wno-unknown-pragmas"
open_solution "solution1"
set_part {xc7k160tfbg484-2}
create_clock -period 10 -name default
source "./selector_prj/solution1/directives.tcl"
csim_design
csynth_design
cosim_design
export_design -format ip_catalog
