# Vivado High level Synthesis

is a tool that allows designers to develop and verify a FPGA algorithm in a higher level abstraction,
without previous knowledge on the hardware description languages.

# vivado_hls_project

Using Vivado HLS tool, this project takes in AntiKt clustered jet pts and counts the number of jets above a certain pt threshold per event.

fir_test.c is the main program that reads in the data, count the jets, write out the answer then compare with the golden result.
selector.c is the top level function the preforms the pt based selection;
run_hls.tcl is a script that contains vivado hls commands. These commands add input files and walk through steps of the design flow.

The Synthesis reports and the automatically generated files in hardware description languages are under selector_prj/solution*.
