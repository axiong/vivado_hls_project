############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
############################################################
open_project -reset selector_prj1
set_top sel
add_files selector.c
add_files -tb fir_test.c
add_files -tb ./data/n.dat
add_files -tb ./data/pt.dat
add_files -tb ./data/count_gold.dat


# Reset the project with the -reset option
open_solution -reset "solution1"
set_part {xc7k160tfbg484-2}
create_clock -period 10 -name default

#source "./fir_prj/solution1/directives.tcl"
csim_design
csynth_design
cosim_design
export_design -rtl verilog -format ip_catalog

exit
